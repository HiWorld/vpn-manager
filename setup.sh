#!/bin/bash
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update -y
sudo apt-get install certbot zip unzip curl sslh wget -y
#buat dir
sudo mkdir -p /etc/v2ray/vless
sudo mkdir -p /etc/v2ray/trojan
sudo mkdir -p /etc/v2ray/vmess
sudo mkdir /etc/v2ray/config
clear
echo "[!] Installasi certificate ..."
read -p "Masukkan domain Anda: " domain
sudo certbot certonly --standalone --preferred-challenges http --agree-tos --register-unsafely-without-email -d $domain
if [ $? -eq 0 ]
then
    echo "Sertifikat berhasil diperoleh untuk domain $domain."
    echo "$domain" > /etc/v2ray/domain.txt
else
    clear
    echo "Gagal mendapatkan sertifikat untuk domain $domain."
    exit 1
fi
wget https://github.com/v2fly/v2ray-core/releases/latest/download/v2ray-linux-64.zip
unzip v2ray-linux-64.zip -d v2ray
sudo cp v2ray/v2ray /usr/bin/
sudo cp v2ray/v2ctl /usr/bin/
sudo cp v2ray/geoip-only-cn-private.dat /usr/bin/
sudo cp v2ray/geosite.dat /usr/bin/
sudo cp v2ray/geoip.dat /usr/bin
rm -rf v2ray 
rm -f v2ray-linux-64.zip
#vmess
sudo tee /etc/v2ray/config/config.json <<EOF
{
  "log": {
  "access": "/var/log/v2ray/access.log",
  "error": "/var/log/v2ray/error.log",
  "loglevel": "warning"
  },
  "inbounds": [
    {
      "listen": "127.0.0.1",
      "port": 8081,
      "protocol": "vmess",
      "settings": {
        "clients": [
        {
            "id": "1342284b-b0bb-4f79-b901-40e2a7e90e39",
            "alterId": 0
          }
        ],
        "disableInsecureEncryption": true
      },
      "streamSettings": {
        "network": "ws",
        "wsSettings": {
          "path": "/ws"
        }
      }
    },
    {
      "listen": "127.0.0.1",
      "port": 8082,
      "protocol": "vmess",
      "settings": {
        "clients": [
        {
            "id": "1342284b-b0bb-4f79-b901-40e2a7e90e39",
            "alterId": 0
          }
        ],
        "disableInsecureEncryption": true
      },
      "streamSettings": {
        "network": "ws",
        "security": "tls",
        "tlsSettings": {
          "certificates": [
            {
              "certificateFile": "/etc/letsencrypt/live/${domain}/fullchain.pem",
              "keyFile": "/etc/letsencrypt/live/${domain}/privkey.pem"
            }
          ]
        },
        "wsSettings": {
          "path": "/wstls"
        }
      }
    }
  ],
  "outbounds": [
    {
      "protocol": "freedom",
      "settings": {}
    }
  ]
}
EOF
#vless
sudo tee /etc/v2ray/config/vless.json <<EOF
{
  "log": {
    "access": "/var/log/v2ray/access.log",
    "error": "/var/log/v2ray/error.log",
    "loglevel": "warning"
  },
  "inbounds": [
    {
      "port": 8083,
      "protocol": "vless",
      "settings": {
        "clients": [
        {
            "id": "4f1f0428-7483-4958-b70f-f0ce400593cc",
            "level": 0
          }
        ],
        "decryption": "none"
      },
      "streamSettings": {
        "network": "ws",
        "wsSettings": {
          "path": "/vlessws",
          "headers": {
            "Host": ""
          }
        }
      }
    },
    {
      "port": 8084,
      "protocol": "vless",
      "settings": {
        "clients": [
        {
            "id": "4f1f0428-7483-4958-b70f-f0ce400593cc",
            "level": 0
          }
        ],
        "decryption": "none"
      },
      "streamSettings": {
        "network": "ws",
        "security": "tls",
        "tlsSettings": {
          "certificates": [
            {
              "certificateFile": "/etc/letsencrypt/live/${domain}/fullchain.pem",
              "keyFile": "/etc/letsencrypt/live/${domain}/privkey.pem"
            }
          ]
        },
        "wsSettings": {
          "path": "/vlesstls",
          "headers": {
            "Host": ""
          }
        }
      }
    }
  ],
  "outbounds": [
    {
      "protocol": "freedom",
      "settings": {}
    }
  ]
}
EOF
#trojan
sudo tee /etc/v2ray/config/trojan.json <<EOF
{
  "log": {
    "loglevel": "warning",
    "access": "/var/log/v2ray/access.log",
    "error": "/var/log/v2ray/error.log"
  },
  "inbounds": [
    {
      "listen": "0.0.0.0",
      "port": 8085,
      "protocol": "trojan",
      "settings": {
        "clients": [
        {
            "password": "fSogCBVwH5JIg_sHJdwuSg",
            "level": 8
          }
        ],
        "fallbacks": [
          {
            "dest": "www.google.com:443"
          }
        ]
      },
      "streamSettings": {
        "network": "grpc",
        "grpcSettings": {
          "serviceName": "trgrpc"
        },
        "security": "tls",
        "tlsSettings": {
          "certificates": [
            {
              "certificateFile": "/etc/letsencrypt/live/${domain}/fullchain.pem",
              "keyFile": "/etc/letsencrypt/live/${domain}/privkey.pem"
            }
          ]
        }
      },
      "tag": "trojan-inbound"
    }
  ],
  "outbounds": [
    {
      "protocol": "freedom",
      "settings": {},
      "tag": "direct"
    }
  ],
  "routing": {
    "rules": [
      {
        "type": "field",
        "outboundTag": "direct",
        "ip": [
          "geoip:private"
        ]
      }
    ]
  }
}
EOF
# Buat direktori log
sudo mkdir -p /var/log/v2ray/
sudo tee /etc/systemd/system/v2ray.service <<EOF
[Unit]
Description=V2Ray Service
After=network.target

[Service]
User=root
Group=root
Restart=always
ExecStart=/usr/bin/v2ray run -config /etc/v2ray/config/config.json
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF
sudo tee /etc/systemd/system/v2ray@.service <<EOF
[Unit]
Description=V2Ray Service
After=network.target

[Service]
User=root
Group=root
Restart=always
ExecStart=/usr/bin/v2ray run -config /etc/v2ray/config/%i.json
RestartSec=10s

[Install]
WantedBy=multi-user.target
EOF
clear
systemctl daemon-reload
systemctl enable v2ray
systemctl enable v2ray@vless
systemctl enable v2ray@trojan
systemctl start v2ray
systemctl start v2ray@vless
systemctl start v2ray@trojan
#auto delete
sudo timedatectl set-timezone Asia/Jakarta
sudo timedatectl set-ntp true
echo "59 23 * * * root /etc/v2ray/auto-exp" | sudo tee -a /etc/crontab > /dev/null
syatemctl restart cron
#setup sslh
sudo tee /etc/default/sslh <<EOF
# Default options for sslh initscript
# sourced by /etc/init.d/sslh

# binary to use: forked (sslh) or single-thread (sslh-select) version
# systemd users: don't forget to modify /lib/systemd/system/sslh.service
DAEMON=/usr/sbin/sslh
#https
DAEMON_OPTS="--user sslh --listen 0.0.0.0:443 --anyprot 127.0.0.1:8082 --anyprot 127.0.0.1:8084 --anyprot 127.0.0.1:8085 --pidfile /var/run/sslh/sslh.pid"
#http
DAEMON_OPTS="\$DAEMON_OPTS --listen 0.0.0.0:80 --anyprot 127.0.0.1:8081 --anyprot 127.0.0.1:8083"
#tambahan
DAEMON_OPTS="\$DAEMON_OPTS --listen 0.0.0.0:8443 --anyprot 127.0.0.1:8082 --anyprot 127.0.0.1:8084 --anyprot 127.0.0.1:8085
DAEMON_OPTS="\$DAEMON_OPTS --listen 0.0.0.0:8880 --anyprot 127.0.0.1:8081 --anyprot 127.0.0.1:8083"
DAEMON_OPTS="\$DAEMON_OPTS --listen 0.0.0.0:2087 --anyprot 127.0.0.1:8082 --anyprot 127.0.0.1:8084 --anyprot 127.0.0.1:8085
DAEMON_OPTS="\$DAEMON_OPTS --listen 0.0.0.0:2086 --anyprot 127.0.0.1:8081 --anyprot 127.0.0.1:8083"
EOF
systemctl enable sslh
systemctl start sslh
#install menu
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/add-trojan
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/add-vmess
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/add-vless
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/del-trojan
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/del-vmess
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/del-vless
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/chk-trojan
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/chk-vmess
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/chk-vless
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/menu-trojan
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/menu-vmess
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/menu-vless
wget -P /usr/bin https://gitlab.com/HiWorld/vpn-manager/-/raw/main/menu
wget -O /usr/bin/speedtest https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py
wget -P /etc/v2ray https://gitlab.com/HiWorld/vpn-manager/-/raw/main/auto-exp
chmod +x /usr/bin/add-trojan
chmod +x /usr/bin/add-vmess
chmod +x /usr/bin/add-vless
chmod +x /usr/bin/del-trojan
chmod +x /usr/bin/del-vmess
chmod +x /usr/bin/del-vless
chmod +x /usr/bin/chk-trojan
chmod +x /usr/bin/chk-vmess
chmod +x /usr/bin/chk-vless
chmod +x /usr/bin/menu-trojan
chmod +x /usr/bin/menu-vmess
chmod +x /usr/bin/menu-vless
chmod +x /usr/bin/menu
chmod +x /usr/bin/speedtest
chmod +x /etc/v2ray/auto-exp
clear
echo "[!] Installasi selesai silahkan ketik menu untuk menampilkan menu..."